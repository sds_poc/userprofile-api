﻿using System.IO;
using System.Net;
using System;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using userprofile.poc.Models;
using userprofile.poc.Services;
using System.Threading.Tasks;

namespace userprofile.poc.API
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    [RoutePrefix("api/UserProfile")]
    public class UserProfileController : ApiController
    {
        private UserProfileService _userProfileService;

        public UserProfileController()
        {
            _userProfileService = new UserProfileService();
        }

        [HttpPost]
        public UserProfileResult Post([FromBody] UserProfile userInfo)
        {
            return _userProfileService.CreateOrUpdateUser(userInfo);
        }

        [HttpGet]
        public UserProfileResponse Get()
        {
            string scheme = Request.RequestUri.Scheme;
            string domain = Request.RequestUri.Host;
            int port = Request.RequestUri.Port;

            string currentUrl = $"{scheme}://{domain}:{port}";

            return _userProfileService.GetAllUsers(currentUrl);
        }

        [HttpDelete]
        public UserProfileResult Delete(int userId)
        {
            return _userProfileService.DeleteUser(userId);
        }

        [Route("upload/{userId}")]
        [HttpPost]

        public async Task<UserProfileResult> UploadPhoto(int userId)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                return new UserProfileResult() { statusCode = 1, statusDescription = "Unsupported media type", value = null };
            }

            try
            {
                string FILE_PATH = HttpContext.Current.Server.MapPath("~/Uploads_Temp/");

                if (!Directory.Exists(FILE_PATH))
                {
                    Directory.CreateDirectory(FILE_PATH);
                }
                var uploadPath = FILE_PATH;
                var provider = new MultipartFormDataStreamProvider(uploadPath);
                await Request.Content.ReadAsMultipartAsync(provider);
                var file = provider.FileData[0];
                //var fileName = userId.ToString();
                //var filePath = Path.Combine(uploadPath, fileName);
                //File.Move(file.LocalFileName, filePath);

                //return Request.CreateResponse(HttpStatusCode.OK, "File uploaded successfully");

                return _userProfileService.UploadPhoto(userId, file, file.LocalFileName);
            }
            catch (Exception ex)
            {
                return new UserProfileResult() { statusCode = 1, statusDescription = "Internal Server Error", value = null };
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
