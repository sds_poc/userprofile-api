﻿namespace userprofile.poc.Models
{
    public class UserProfileResult
    {
        public int? value { get; set; }
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
    }
}