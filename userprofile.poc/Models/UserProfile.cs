﻿namespace userprofile.poc.Models
{
    public class UserProfile
    {
        public int? userId { get; set; }
        public string name { get; set; }
        public string emailId { get; set; }
        public string password { get; set; }
        public string profileURL { get; set; }
        public string bio { get; set; }
    }
}