﻿using System.Collections.Generic;

namespace userprofile.poc.Models
{
    public class UserProfileResponse
    {
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public List<UserProfile> users { get; set; }

        public UserProfileResponse()
        {
            users = new List<UserProfile>();
        }
    }
}