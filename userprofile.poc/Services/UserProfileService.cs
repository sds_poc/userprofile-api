﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using userprofile.poc.Helpers;
using userprofile.poc.Models;

namespace userprofile.poc.Services
{
    public class UserProfileService : BaseService
    {
        private string FILE_PATH = HttpContext.Current.Server.MapPath("~/Photos/");

        string connectionString = string.Empty;

        public UserProfileService() : base()
        {
            connectionString = createDBConnection();
        }

        public UserProfileResponse GetAllUsers(string currentUrl)
        {
            UserProfileResponse result = new UserProfileResponse();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlTransaction transaction;
                connection.Open();
                transaction = connection.BeginTransaction();

                StringBuilder sbUserUpdate = new StringBuilder();

                sbUserUpdate.Append("SELECT userId, name, emailId, password, bio FROM UserProfile;");
                string selectSql = sbUserUpdate.ToString();

                try
                {
                    using (SqlCommand command = new SqlCommand(selectSql, connection))
                    {
                        command.Transaction = transaction;

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    UserProfile _userProfile = new UserProfile();
                                    _userProfile.userId = (int)dataReader[0];
                                    _userProfile.name = (string)dataReader[1];
                                    _userProfile.emailId = (string)dataReader[2];
                                    _userProfile.password = (string)dataReader[3];
                                    _userProfile.bio = (string)dataReader[4];
                                    _userProfile.profileURL = GetProfilePhotoFromFolder(currentUrl, _userProfile.userId.ToString());

                                    result.users.Add(_userProfile);
                                }
                            }

                            dataReader.Close();
                        }
                    }

                    transaction.Commit();

                    result.statusDescription = "Retrieved Successfully";
                    result.statusCode = 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.statusDescription = "Error occured while reading user profile";
                    result.statusCode = 1;
                }
            }

            return result;
        }

        private string GetProfilePhotoFromFolder(string currentUrl, string userId)
        {
            var fullPath = CheckFileExistsInFolder(FILE_PATH, userId);

            if (fullPath != string.Empty)
            {

                string[] splittedPathInfo = fullPath.Split(new string[] { "\\" }, StringSplitOptions.None);

                return $"{currentUrl}/Photos/{splittedPathInfo[splittedPathInfo.Length - 1]}";
            }

            return string.Empty;
        }

        public UserProfileResult CreateOrUpdateUser(UserProfile userProfile)
        {
            if (userProfile.userId != null)
            {
                return UpdateUser(userProfile);
            }
            else
            {
                return CreateUser(userProfile);
            }
        }

        private UserProfileResult CreateUser(UserProfile userProfile)
        {
            UserProfileResult result = new UserProfileResult();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlTransaction transaction;
                connection.Open();
                transaction = connection.BeginTransaction();

                StringBuilder sbUserInsert = new StringBuilder();
                StringBuilder sbEmailOrUsernameValidation = new StringBuilder();

                sbUserInsert.Append("INSERT UserProfile (name, emailId, password, bio) VALUES (@name, @email, @password, @bio);");
                string insertSql = sbUserInsert.ToString();

                sbEmailOrUsernameValidation.Append("SELECT * from UserProfile WHERE emailId = @email");
                string validateSql = sbEmailOrUsernameValidation.ToString();

                try
                {
                    int? userId = null;

                    using (SqlCommand command = new SqlCommand(validateSql, connection))
                    {
                        command.Transaction = transaction;
                        command.Parameters.AddWithValue("@email", userProfile.emailId);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                connection.Close();

                                return new UserProfileResult() { statusCode = 1, statusDescription = "Email already exists" };
                            }
                        }
                    }

                    string encryptedPassword = PasswordHelper.EncodePasswordToBase64(userProfile.password);

                    using (SqlCommand command = new SqlCommand(insertSql, connection))
                    {
                        command.Transaction = transaction;
                        command.Parameters.AddWithValue("@name", userProfile.name);
                        command.Parameters.AddWithValue("@email", userProfile.emailId);
                        command.Parameters.AddWithValue("@password", encryptedPassword);
                        command.Parameters.AddWithValue("@bio", userProfile.bio);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected == 1)
                        {
                            using (SqlCommand selectCommand = new SqlCommand(validateSql, connection))
                            {
                                selectCommand.Transaction = transaction;
                                selectCommand.Parameters.AddWithValue("@email", userProfile.emailId);

                                using (SqlDataReader reader = selectCommand.ExecuteReader())
                                {
                                    if (reader.HasRows)
                                    {
                                        reader.Read();
                                        userId = (int)reader[0];
                                    }

                                    reader.Close();
                                }
                            }
                        }
                    }

                    transaction.Commit();

                    result.value = userId;
                    result.statusDescription = "Created User";
                    result.statusCode = 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.statusDescription = "Error occured while creating user profile";
                    result.statusCode = 1;
                }

                connection.Close();
            }

            return result;
        }

        private UserProfileResult UpdateUser(UserProfile userProfile)
        {
            UserProfileResult result = new UserProfileResult();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlTransaction transaction;
                connection.Open();
                transaction = connection.BeginTransaction();

                StringBuilder sbUserUpdate = new StringBuilder();
                StringBuilder sbEmailOrUsernameValidation = new StringBuilder();

                sbUserUpdate.Append("UPDATE UserProfile SET name = @name, emailId = @email, bio = @bio");

                if (userProfile.password != null && userProfile.password != string.Empty)
                {
                    sbUserUpdate.Append(", password = @password");
                }

                sbUserUpdate.Append(" WHERE userId = @id;");
                string updateSql = sbUserUpdate.ToString();

                sbEmailOrUsernameValidation.Append("SELECT * from UserProfile WHERE emailId = @email AND userId != @id;");
                string validateSql = sbEmailOrUsernameValidation.ToString();

                try
                {
                    using (SqlCommand command = new SqlCommand(validateSql, connection))
                    {
                        command.Transaction = transaction;
                        command.Parameters.AddWithValue("@email", userProfile.emailId);
                        command.Parameters.AddWithValue("@id", userProfile.userId);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                connection.Close();

                                return new UserProfileResult { statusCode = 1, statusDescription = "Email already exists" };
                            }
                        }
                    }

                    using (SqlCommand command = new SqlCommand(updateSql, connection))
                    {
                        command.Transaction = transaction;
                        command.Parameters.AddWithValue("@name", userProfile.name);

                        if (userProfile.password != null && userProfile.password != string.Empty)
                        {
                            string encryptedPassword = PasswordHelper.EncodePasswordToBase64(userProfile.password);
                            command.Parameters.AddWithValue("@password", encryptedPassword);
                        }

                        command.Parameters.AddWithValue("@email", userProfile.emailId);
                        command.Parameters.AddWithValue("@id", userProfile.userId);
                        command.Parameters.AddWithValue("@bio", userProfile.bio);

                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();

                    result.value = userProfile.userId;
                    result.statusDescription = "Updated Successfully";
                    result.statusCode = 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.statusDescription = "Error occured while updating user profile";
                    result.statusCode = 1;
                }

                connection.Close();
            }

            return result;
        }

        public UserProfileResult DeleteUser(int userId)
        {
            UserProfileResult result = new UserProfileResult();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlTransaction transaction;
                connection.Open();
                transaction = connection.BeginTransaction();

                StringBuilder sbUserdelete = new StringBuilder();

                sbUserdelete.Append("DELETE FROM UserProfile WHERE userId = @userId");
                string insertSql = sbUserdelete.ToString();

                try
                {
                    using (SqlCommand command = new SqlCommand(insertSql, connection))
                    {
                        command.Transaction = transaction;
                        command.Parameters.AddWithValue("@userId", userId);

                        command.ExecuteNonQuery();
                    }

                    string fileName = userId.ToString();

                    if (!Directory.Exists(FILE_PATH))
                    {
                        Directory.CreateDirectory(FILE_PATH);
                    }
                    var existingFilePath = CheckFileExistsInFolder(FILE_PATH, fileName);

                    if (existingFilePath != string.Empty)
                    {
                        File.Delete(existingFilePath);
                    }

                    transaction.Commit();

                    result.statusDescription = "Deleted User Profile";
                    result.statusCode = 0;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    result.statusDescription = "Error occurred while deleting user profile";
                    result.statusCode = 1;
                }

                connection.Close();
            }

            return result;
        }

        public UserProfileResult UploadPhoto(int userId, MultipartFileData file, string localFileName)
        {
            UserProfileResult result = new UserProfileResult();

            try
            {
                string fileName = userId.ToString();

                string _fileNameForExtension = file.Headers.ContentDisposition.FileName.Trim('"');

                string[] splittedString = _fileNameForExtension.Split('.');

                string _extension = splittedString[splittedString.Length - 1];

                if (!Directory.Exists(FILE_PATH))
                {
                    Directory.CreateDirectory(FILE_PATH);
                }
                var existingFilePath = CheckFileExistsInFolder(FILE_PATH, fileName);

                if (existingFilePath != string.Empty)
                {
                    File.Delete(existingFilePath);
                }

                File.Move(localFileName ,HttpContext.Current.Server.MapPath("~/Photos/" + fileName + "." + _extension));

                result.statusCode = 0;
                result.statusDescription = "Uploaded successfully";

            }
            catch (Exception ex)
            {
                result.statusCode = 1;
                result.statusDescription = "Error occurred while uploading file";
            }

            return result;
        }

        private string CheckFileExistsInFolder(string folderPath, string fileNameToSearch)
        {
            try
            {
                string[] files = Directory.GetFiles(folderPath, "*", SearchOption.TopDirectoryOnly);

                foreach (string filePath in files)
                {
                    string fileName = Path.GetFileNameWithoutExtension(filePath);

                    if (string.Equals(fileName, fileNameToSearch, StringComparison.OrdinalIgnoreCase))
                    {
                        return filePath;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }

            return string.Empty;
        }
    }
}